#include <stdio.h>
#include <stdlib.h>
#include "Vexample_and_gate.h"
#include "verilated.h"

int main(int argc, char **argv) {
Verilated::commandArgs(argc, argv);
Vexample_and_gate *tb = new Vexample_and_gate;

for(int k=0; k<2;k++){
tb->input_1 = k&1;
for(int l=0; l<2;l++){
tb->input_2 = l&1;

tb->eval();

printf("k = %2d, ", k);
printf("input_1 = %d, ", tb->input_1);
printf("input_2 = %d, ", tb->input_2);
printf("and_result = %d\n", tb->and_result);
}
}
}

