// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Symbol table internal header
//
// Internal details; most calling programs do not need this header,
// unless using verilator public meta comments.

#ifndef _VEXAMPLE_AND_GATE__SYMS_H_
#define _VEXAMPLE_AND_GATE__SYMS_H_  // guard

#include "verilated.h"

// INCLUDE MODULE CLASSES
#include "Vexample_and_gate.h"

// SYMS CLASS
class Vexample_and_gate__Syms : public VerilatedSyms {
  public:
    
    // LOCAL STATE
    const char* __Vm_namep;
    bool __Vm_didInit;
    
    // SUBCELL STATE
    Vexample_and_gate*             TOPp;
    
    // CREATORS
    Vexample_and_gate__Syms(Vexample_and_gate* topp, const char* namep);
    ~Vexample_and_gate__Syms() {}
    
    // METHODS
    inline const char* name() { return __Vm_namep; }
    
} VL_ATTR_ALIGNED(VL_CACHE_LINE_BYTES);

#endif  // guard
