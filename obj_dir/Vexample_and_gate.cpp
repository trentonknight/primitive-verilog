// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vexample_and_gate.h for the primary calling header

#include "Vexample_and_gate.h"
#include "Vexample_and_gate__Syms.h"

//==========

VL_CTOR_IMP(Vexample_and_gate) {
    Vexample_and_gate__Syms* __restrict vlSymsp = __VlSymsp = new Vexample_and_gate__Syms(this, name());
    Vexample_and_gate* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Reset internal values
    
    // Reset structure values
    _ctor_var_reset();
}

void Vexample_and_gate::__Vconfigure(Vexample_and_gate__Syms* vlSymsp, bool first) {
    if (false && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
    if (false && this->__VlSymsp) {}  // Prevent unused
    Verilated::timeunit(-12);
    Verilated::timeprecision(-12);
}

Vexample_and_gate::~Vexample_and_gate() {
    VL_DO_CLEAR(delete __VlSymsp, __VlSymsp = NULL);
}

void Vexample_and_gate::eval_step() {
    VL_DEBUG_IF(VL_DBG_MSGF("+++++TOP Evaluate Vexample_and_gate::eval\n"); );
    Vexample_and_gate__Syms* __restrict vlSymsp = this->__VlSymsp;  // Setup global symbol table
    Vexample_and_gate* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
#ifdef VL_DEBUG
    // Debug assertions
    _eval_debug_assertions();
#endif  // VL_DEBUG
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    int __VclockLoop = 0;
    QData __Vchange = 1;
    do {
        VL_DEBUG_IF(VL_DBG_MSGF("+ Clock loop\n"););
        _eval(vlSymsp);
        if (VL_UNLIKELY(++__VclockLoop > 100)) {
            // About to fail, so enable debug to see what's not settling.
            // Note you must run make with OPT=-DVL_DEBUG for debug prints.
            int __Vsaved_debug = Verilated::debug();
            Verilated::debug(1);
            __Vchange = _change_request(vlSymsp);
            Verilated::debug(__Vsaved_debug);
            VL_FATAL_MT("example_and_gate.v", 1, "",
                "Verilated model didn't converge\n"
                "- See DIDNOTCONVERGE in the Verilator manual");
        } else {
            __Vchange = _change_request(vlSymsp);
        }
    } while (VL_UNLIKELY(__Vchange));
}

void Vexample_and_gate::_eval_initial_loop(Vexample_and_gate__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    _eval_initial(vlSymsp);
    // Evaluate till stable
    int __VclockLoop = 0;
    QData __Vchange = 1;
    do {
        _eval_settle(vlSymsp);
        _eval(vlSymsp);
        if (VL_UNLIKELY(++__VclockLoop > 100)) {
            // About to fail, so enable debug to see what's not settling.
            // Note you must run make with OPT=-DVL_DEBUG for debug prints.
            int __Vsaved_debug = Verilated::debug();
            Verilated::debug(1);
            __Vchange = _change_request(vlSymsp);
            Verilated::debug(__Vsaved_debug);
            VL_FATAL_MT("example_and_gate.v", 1, "",
                "Verilated model didn't DC converge\n"
                "- See DIDNOTCONVERGE in the Verilator manual");
        } else {
            __Vchange = _change_request(vlSymsp);
        }
    } while (VL_UNLIKELY(__Vchange));
}

VL_INLINE_OPT void Vexample_and_gate::_combo__TOP__1(Vexample_and_gate__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vexample_and_gate::_combo__TOP__1\n"); );
    Vexample_and_gate* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->and_result = ((IData)(vlTOPp->input_1) 
                          & (IData)(vlTOPp->input_2));
}

void Vexample_and_gate::_eval(Vexample_and_gate__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vexample_and_gate::_eval\n"); );
    Vexample_and_gate* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_combo__TOP__1(vlSymsp);
}

void Vexample_and_gate::_eval_initial(Vexample_and_gate__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vexample_and_gate::_eval_initial\n"); );
    Vexample_and_gate* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void Vexample_and_gate::final() {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vexample_and_gate::final\n"); );
    // Variables
    Vexample_and_gate__Syms* __restrict vlSymsp = this->__VlSymsp;
    Vexample_and_gate* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void Vexample_and_gate::_eval_settle(Vexample_and_gate__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vexample_and_gate::_eval_settle\n"); );
    Vexample_and_gate* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_combo__TOP__1(vlSymsp);
}

VL_INLINE_OPT QData Vexample_and_gate::_change_request(Vexample_and_gate__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vexample_and_gate::_change_request\n"); );
    Vexample_and_gate* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // Change detection
    QData __req = false;  // Logically a bool
    return __req;
}

#ifdef VL_DEBUG
void Vexample_and_gate::_eval_debug_assertions() {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vexample_and_gate::_eval_debug_assertions\n"); );
    // Body
    if (VL_UNLIKELY((input_1 & 0xfeU))) {
        Verilated::overWidthError("input_1");}
    if (VL_UNLIKELY((input_2 & 0xfeU))) {
        Verilated::overWidthError("input_2");}
}
#endif  // VL_DEBUG

void Vexample_and_gate::_ctor_var_reset() {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vexample_and_gate::_ctor_var_reset\n"); );
    // Body
    input_1 = VL_RAND_RESET_I(1);
    input_2 = VL_RAND_RESET_I(1);
    and_result = VL_RAND_RESET_I(1);
}
