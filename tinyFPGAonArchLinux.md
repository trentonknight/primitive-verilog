# Serial port access
```bash
gpasswd -a username uucp
```
# Software
## Icestudio
Download the stable AppImage here:
[icestudio stable](https://github.com/FPGAwars/icestudio/releases)
## CLI tools
```bash
pacman -Sy python-pip
pip install apio tinyprog
```
### Tinyprog
```bash
tinyprog --update-bootloader
```
### APIO CLI tool
View all apio commands by entering command without arguments or --help.
```bash
apio
apio --help
```
```bash
apio install system scons ice40 iverilog
```
Or install all packages
```bash
apio install --all
```
### Init 
Before running init you may want to create a new directory for your project
#### Examples
Init for TinyFPGA BX with no example files
```bash
apio init --board TinyFPGA-BX
``` 
Create standard template
```bash
apio examples -f TinyFPGA-BX/template
```
#### Check code
```bash
apio verify
```
#### Verify Verilog with GTKWave
```bash
apio sim
```
#### Yosys view flowchart
```bash
yosys
read -sv TinyFPGA_BX.v
show
```
#### Synthesize project with Icestorm Tools
```bash
apio build
```
#### Connect to TinyFPGA and upload bitstream
```bash
sudo tinyprog -p hardware.bin
```
THe following does not appear to be working as of 29AUG20?
```bash
apio upload
```

# Repositories
```bash
https://github.com/FPGAwars
https://github.com/tinyfpga/TinyFPGA-BX.git
```
# Useful tutorials
* [Official Guide](https://tinyfpga.com/bx/guide.html)
* [Instructables](https://www.instructables.com/id/Getting-Started-With-the-Tinyfpga/)
* [Icestudio](https://icestudio.io/)
* [apio](https://apiodoc.readthedocs.io/en/stable/source/quick_start.html)
