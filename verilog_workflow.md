# Verilog Workflow
## verilog with yosys for .dot and graphviz type schematics
```verilog
///////////////////////////////////////////////////////////////////////////////
// File Downloaded from http://www.nandland.com
///////////////////////////////////////////////////////////////////////////////
module example_and_gate 
  ( 
    input_1,
    input_2,
    and_result);
   
  input  input_1;
  input  input_2;
  output and_result;
 
  wire   and_temp;  
 
  assign and_temp = input_1 & input_2;
   
  assign and_result = and_temp;
 
endmodule // example_and_gate
```
Use Yosys to display netlist:
``` bash
yosys> read -sv example_and_gate.v
yosys> show
```
Show using additional formats:
```bash
yosys> show -format ps -viewer evince
```
## Using verilator to simulate AND gate
```bash
verilator -Wall -cc example_and_gate.v
cd obj_dir/
make -f Vexample_and_gate.mk
```
Write simulator:example_and_gate.cpp
```cpp
#include <stdio.h>
#include <stdlib.h>
#include "Vexample_and_gate.h"
#include "verilated.h"

int main(int argc, char **argv) {
Verilated::commandArgs(argc, argv);
Vexample_and_gate *tb = new Vexample_and_gate;

for(int k=0; k<2;k++){
tb->input_1 = k&1;
for(int l=0; l<2;l++){
tb->input_2 = l&1;

tb->eval();

printf("k = %2d, ", k);
printf("input_1 = %d, ", tb->input_1);
printf("input_2 = %d, ", tb->input_2);
printf("and_result = %d\n", tb->and_result);
}
}
}
```
Compile using verilator:
``` bash
++ -I /usr/share/verilator/include -I ../obj_dir/ /usr/share/verilator/include/verilated.cpp example_and_gate.cpp ../obj_dir/Vexample_and_gate__ALL.a -o example_and_gate
```
## Run simulation of AND gate:
```bash
./example_and_gate
```
## Nested Loop inputs
The verilator simulation is using nested loops adding power to input_1 and switching the input_2 off an on for each iteration. Another words input_1 is turned off or on once per interation while input_2 is switched both off and on:

``` bash
k =  0, input_1 = 0, input_2 = 0, and_result = 0
k =  0, input_1 = 0, input_2 = 1, and_result = 0
k =  1, input_1 = 1, input_2 = 0, and_result = 0
k =  1, input_1 = 1, input_2 = 1, and_result = 1
```
# VTR (verilog-to-routing)
Install using pacman
```
pacman -Sy vtr
```
## Install VTR from source code 
### Install dependancies first
``` bash
pacman -Sy cmake clang bison flex gcc llvm cairo freetype Xft ctags gdb valgrind doxygen python-spinx imagemagick texlive-latexextra python-spinx python-spinx_rtd_theme python-recommonmark tbb
```
### Download the latest release of VTR:

Stable release may be downloaded or use git clone for the latest source code.

* [VTR stable release](https://github.com/verilog-to-routing/vtr-verilog-to-routing/releases)
* [verilogtorouting.org](https://verilogtorouting.org/)

### VTR git clone
``` bash
git clone https://github.com/verilog-to-routing/vtr-verilog-to-routing
```
#### Build with CMake from build directory
``` bash
cd vtr-verilog-to-routing
mkdir build
cd build
cmake ../
make
make install
```
### Append VTR_ROOT PATH to .bashrc
``` bash
export VTR_ROOT=~/Developer/vtr-verilog-to-routing
export PATH=$PATH:$VTR_ROOT/build/bin
```





























